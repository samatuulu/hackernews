from source.webapp.models import Article, Comment, Vote
from rest_framework import serializers


class ArticleSerializer(serializers.ModelSerializer):

	class Meta:
		model = Article
		fields = '__all__'


class CommentSerializer(serializers.ModelSerializer):

	class Meta:
		model = Comment
		fields = '__all__'


class VoteSerializer(serializers.ModelSerializer):

	class Meta:
		model = Vote
		fields = '__all__'
