from django.urls import include, path
from rest_framework import routers

from source.api import views

router = routers.DefaultRouter()

router.register(r'articles', views.ArticleViewSet)
router.register(r'comments', views.CommentViewSet)
router.register(r'vote', views.VoteViewSet)

app_name = 'api'


urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))

]
