from django.views.generic import ListView, DetailView
from django.shortcuts import render
from .models import Article, Comment,Vote


class ArticleListView(ListView):
	model = Article
	template_name = 'article/article_list.html'
	context_object_name = 'articles'


class ArticleDetailView(DetailView):
	model = Article
	template_name = 'article/article_detail.html'
	context_object_name = 'article'
