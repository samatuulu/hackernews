from django.db import models
from django.db import IntegrityError
from django.contrib.auth.models import User


class Article(models.Model):
	author_name = models.CharField(max_length=60)
	title = models.CharField(max_length=500)
	link = models.URLField(max_length=200)
	created_at = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return f"{self.title}"


class Comment(models.Model):
	user = models.ForeignKey(User, on_delete=models.PROTECT, related_name='commented_by')
	article = models.ForeignKey(Article, on_delete=models.CASCADE, related_name='article_comment')
	content = models.TextField(max_length=1500)
	created_at = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return self.content


class Vote(models.Model):
	article = models.ForeignKey(Article, on_delete=models.CASCADE, related_name='article_vote')
	user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_vote')

	def __str__(self):
		return str(self.article.author_name)