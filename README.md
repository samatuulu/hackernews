## HackerNews backend API

## Getting Started

### Docker

I've launched with Pipenv and there are two files on it,
which is generated from the beginning when I created the project.

Open the terminal from project root directly and run command below:

To do this. Type simple command below. 

```shell script
docker-compose up -d --build
```

it will build contianer and runs the server behind it.

Run migrate in docker container as connected to postgresql:
```docker-compose exec web python source/manage.py migrate```

Create superuser:
```docker-compose exec web python source/manage.py createsuperuser```

Finally, project under Docker and connects on port 8000. Now, you can check http://localhost:8000

Just in case, if you want to run project locally, then, please checkout to this commit:

e48ff02fe6c6e9f07c4e8e923344ea20176888b1

the above commits from that, are the commits with Heroku that I solved.

Link to Herokuapp:
https://testprojecthackernews.herokuapp.com/

credentials:
u: admin
p: admin